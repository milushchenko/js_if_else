const min = Math.floor(Math.random() * 60);
let quarter = "";

if (min < 15) {
    quarter = "першої";
} else if (min < 30) {
    quarter = "другої";
} else if (min < 45) {
    quarter = "третьої";
} else if (min < 60) {
    quarter = "четвертої";
}

console.log(`${min} хвилин належить до ${quarter} чвертi`);

let lang = "ua";
let arr = [];

if (lang === "ua") {
    arr = ['Понеділок', 'Вівторок', 'Середа', 'Четвер', 'П`ятниця', 'Субота', 'Неділя'];
} else if (lang === "en") {
    arr = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
}

console.log(arr);


let str = "501042";
let firstThreeDigits = str.substring(0, 3);
let lastThreeDigits = str.substring(str.length - 3);
let sumFirstThreeDigits = 0;
let sumLastThreeDigits = 0;

for (let i = 0; i < 3; i++) {
    sumFirstThreeDigits += parseInt(firstThreeDigits[i]);
    sumLastThreeDigits += parseInt(lastThreeDigits[i]);
}

if (sumFirstThreeDigits === sumLastThreeDigits) {
    console.log("Щасливий!");
} else {
    console.log("Вам не пощастило");
}